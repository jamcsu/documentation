---
editLink: false
nextLinks: false
prevLinks: false
---

# Syntax Legend
- `<>` - Parameters encased by angle brackets are required. The tag will not function unless these parameters are provided.
- `[]` - Parameters encased by square brackets are optional. The tag will still function if these parameters are not provided.
- `()` - Parameters encased by parenthesis have different requirements depending on the context, usually explained in more detail by a notation.
- `...` - Indicates tags that have "infinitely" expandable parameters.