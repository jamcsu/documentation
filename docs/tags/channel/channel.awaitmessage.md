---
tags:
- channel
---

# {channel.awaitMessage}
waits for a message in a channel and returns the message content. If `[author]` is set, it will only accept message from that user. If `[timeout]` is not set, it will default to 30 seconds.

### Context Requirements
- Channel

## Syntax
```
{channel.awaitMessage;[author];[timeout];[channel]}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{channel.awaitMessage}
```
#### Context:
```
wew this is a message
```
#### Output:
```
wew this is a message
```
::: details Explanation
After executing the tag, a user sends the context message ("wew this is a message") within the default `[timeout]` of 30 seconds. Atlas would detect that message then output it.
:::