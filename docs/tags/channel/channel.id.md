---
tags:
- channel
---

# {channel.id}
gets the ID of a channel.

### Context Requirements
- Channel

## Syntax
```
{channel.id;[channel]}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{channel.id}
```
#### Output:
```
357192258434629633
```
**Note:** The output would be the ID of the channel in context.
:::

::: details <b>Example 2</b>
#### Input:
```
{channel.id;chat}
```
#### Output:
```
624637276181495822
```
**Note:** The output would be the ID of the channel in context.
:::