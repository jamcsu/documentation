---
tags:
- channel
---

# {channel.createdAt}
returns the date the channel was created. `[exact]` is a boolean to include hours/minutes.

### Context Requirements
- Channel

## Syntax
```
{channel.createdAt;[exact];[channel]}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{channel.createdAt}
```
#### Output:
```
April 20, 2069
```
**Note:** The output would be the date the channel in context was created.
:::

::: details <b>Example 2</b>
#### Input:
```
{channel.createdAt;true}
```
#### Output:
```
April 20, 2069, 4:20 PM
```
**Note:** The output would be the date and time the channel in context was created.
:::