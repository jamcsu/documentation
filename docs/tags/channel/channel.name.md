---
tags:
- channel
---

# {channel.name}
gets the name of a channel.

### Context Requirements
- Channel

## Syntax
```
{channel.name;[channel]}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{channel.name}
```
### Output:
```
general
```
**Note:** The context for this example would be the channel that the tag was run in since no `[channel]` argument was defined.
:::

::: details <b>Example 2</b>
#### Input:
```
{channel.name;452552369767579659}
```
### Output:
```
rules
```
:::