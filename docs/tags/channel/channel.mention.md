---
tags:
- channel
---

# {channel.mention}
gets the `#mention` of a channel.

### Context Requirements
- Channel

## Syntax
```
{channel.mention;[channel]}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{channel.mention}
```
### Output:
```
#general
```
**Note:** The context for this example would be the channel that the tag was run in since no `[channel]` argument was defined.
:::

::: details <b>Example 2</b>
#### Input:
```
{channel.mention;memes}
```
### Output:
```
#memes
```
:::