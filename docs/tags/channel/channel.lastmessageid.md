---
tags:
- channel
---

# {channel.lastMessageID}
gets the ID of the last message sent in the channel.

### Context Requirements
- Channel

## Syntax
```
{channel.lastMessageID;[channel]}
```

## Examples
::: details <b>Example 1</b>
#### Context:
```
Yeet, this is a random message sent in a channel.
```
#### Input:
```
{channel.lastMessageID}
```
### Output:
```
705435557064343632
```
**Note:** The output would be the ID of the message in context.
:::