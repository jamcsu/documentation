---
tags:
- global
- casing
---

# {lower}
converts `<string>` to all lowercase. See also [{upper}](upper)

## Syntax
```
{lower;<string>}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{lower;SOMEBODY ONCE TOLD ME}
```
#### Output:
```
somebody once told me
```
:::