---
tags:
- global
- logic
---

# {or}
returns the first parameter that exists and is not empty.

## Syntax
```
{or;<parameterOne>;[parameterTwo];...}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{or;;;test;test again}
```
#### Output:
```
test
```
:::

::: details <b>Example 2</b>
#### Input:
```
{or;uno;dos;tres}
```
#### Output:
```
uno
```
:::