---
tags:
- global
- variable
- volatile
- temporary
---

# {get}
pulls a variable from volatile storage. Used in conjuction with [{set}](set). For persistant storage, use [{perget}](perget)

## Syntax
```
{get;<key>}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{set;yeet;Secret string to store for later} The string we stored is: {get;yeet}
```
#### Output:
```
The string we stored is: Secret string to store for later
```
See documentation for [{set}](set)
:::

::: details <b>Example 2</b>
#### Input:
```
{set;var1;yeet}{set;var2;yote}
```
#### Input (executed from another script):
```
I like to {get;var1} and {get;var2}.
```
#### Output:
```
I like to and .
```
Because the **{get}** was executed in another script, the variable had already been discarded. In this case, you would've needed to use [{perset}](perset). See also documentation for [{set}](set)
:::