---
tags:
- global
- error
---

# {throw}
intentionally throws a tag error. Mainly useful for testing purposes.

## Syntax
```
{throw;[message]}
```

# Examples
::: details <b>Example 1</b>
#### Input:
```
{throw}
```
#### Output:
```
{throw-ERROR1-something-bad-happened}
```
:::

::: details <b>Example 2</b>
#### Input:
```
{throw;Ya done goofed}
```
#### Output:
```
{throw-ERROR1-ya-done-goofed}
```
:::