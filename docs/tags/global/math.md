---
tags:
- global
---

# {math}
evaluates mathematical expressions.

## Syntax
```
{math;<expression>}
```

## Operators
- `+` | Addition
- `-` | Subtraction
- `*` | Multiplication
- `/` | Division
- `%` | Modulo
- `^` | Exponentiation
- `!` | Factorial
- `sqrt()` | Square Root

## Examples
::: details <b>Example 1</b>
#### Input:
```
{math;9+10}
```
#### Output:
```
21
```
:::

::: details <b>Example 2</b>
#### Input:
```
{math;7!}
```
#### Output:
```
5040
```
:::

::: details <b>Example 3</b>
#### Input:
```
{math;(7^2) % 11}
```
#### Output:
```
5
```
:::

::: details <b>Example 4</b>
#### Input:
```
{math;sqrt(9)}
```
#### Output:
```
3
```
:::