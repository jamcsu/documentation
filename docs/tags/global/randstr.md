---
tags:
- global
- random
---

# {randstr}

## Syntax
```
{randstr;[lexicon];(length)}
```
**Note:** `(length)` is optional if `[lexicon]` is defined.

## Examples
::: details <b>Example 1</b>
#### Input:
```
{randstr}
```
#### Output:
```
1q6za2
```
**Note:** Every time the tag is executed, the output may be different.
:::

::: details <b>Example 2</b>
#### Input:
```
{randstr;ABCabc123}
```
#### Output:
```
B1a3aC
```
**Note:** Every time the tag is executed, the output may be different.
:::

::: details <b>Example 3</b>
#### Input:
```
{randstr;abcdefghijklmnopqrstuvwxyzABCDEFGGIJKLMNOPQRSTUVWXYZ01234567890;16}
```
#### Output:
```
VQ7Z3gjLsTeEx7vW
```
**Note:** Every time the tag is executed, the output may be different.
:::