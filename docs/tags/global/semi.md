---
tags:
- global
---

# {semi}
inserts a semicolon (`;`) that won't get eaten by the parser. Useful if you need one in a string or sentence.

## Examples
::: details <b>Example 1</b>
#### Input:
```
You can write {l}tags{semi}with arguments{semi} like this{r} without them being parsed!
```
#### Output:
```
You can write {tags;with arguments;like this} without them being parsed!
```
See documentation for [{l}](l) and [{r}](r)
:::