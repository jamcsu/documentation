---
tags:
- global
- variable
- persistant
- permanent
---

# {perget}
pulls a variable from persistant storage. Used in conjuction with [{perset}](perset). Each guild has a 500 persistant variable limit. For volatile storage, use [{get}](get)

## Syntax
```
{perget;<key>}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{perset;var1;yeet}{perset;var2;yote}
```
#### Input (executed from another script):
```
I like to {perget;var1} and {perget;var2}.
```
#### Output:
```
I like to yeet and yote.
```
**Note:** Because we used persistant variables, we can pull data that was set from other scripts.
:::

::: details <b>Example 2</b>
#### Input:
```
{perset;var1;yeet}{perset;var2;yote}
```
#### Input (executed from another script):
```
I like to {get;var1} and {get;var2}.
```
#### Output:
```
I like to and.
```
**Note:** Here we mixed persistant and temporary variables. Hence, the value was stored successfuly but was not retrieved.
:::