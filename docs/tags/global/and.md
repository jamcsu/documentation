---
tags:
- global
- logic
---

# {and}
returns `true` if all parameters are present, or `false` in any other circumstance.

## Syntax
```
{and;<parameterOne>;[parameterTwo];...}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{and;test;;;test}
```
#### Output:
```
false
```
:::

::: details <b>Example 2</b>
#### Input:
```
{and;test;test;test}
```
#### Output:
```
true
```
:::

::: details <b>Example 3</b>
#### Input:
```
{and;false;false}
```
#### Output:
```
true
```
**Note:** Booleans are not parsed.
:::

::: details <b>Example 4</b>
#### Context:
```
a!command yes no
```
#### Input:
```
{if;{and;{args;1};{args;2}};==;true;succeed;fail}
```
#### Output:
```
succeed
```
See documentation for [{if}](if) and [{args}](args)
::: details Explanation
This bit of code would test to see if both `{args;1}` *and* `{args;2}` existed, then output `succeed` if true or `fail` if false.
:::

::: details <b>Example 5</b>
#### Context:
```
a!command yes
```
#### Input:
```
{if;{and;{args;1};{args;2}};==;true;succeed;fail}
```
#### Output:
```
fail
```
See documentation for [{if}](if) and [{args}](args)
::: details Explanation
This bit of code would test to see if both `{args;1}` *and* `{args;2}` existed, then output `succeed` if true or `fail` if false.
:::