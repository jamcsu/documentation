---
tags:
- global
- random
---

# {range}
returns a random number between `<min>` and `<max>`.

## Syntax
```
{range;<min>;<max>}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{range;5;10}
```
#### Output:
```
7
```
**Note:** Every time the tag is executed, the output may be different.
:::

::: details <b>Example 2</b>
#### Input:
```
{range;0;100}
```
#### Output:
```
69
```
**Note:** Every time the tag is executed, the output may be different.
:::