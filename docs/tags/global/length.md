---
tags:
- global
---

# {length}
returns the numerical length of `<string>`.

## Syntax
```
{length;<string>}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{length;yeehaw}
```
#### Output:
```
6
```
:::

::: details <b>Example 2</b>
#### Input:
```
{length;testing testing testing}
```
#### Output:
```
23
```
:::