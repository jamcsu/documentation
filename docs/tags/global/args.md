---
tags:
- global
---

# {args}
returns a specified argument.

### Context Requirements
- Message

## Syntax
```
{args;[index];[range]}
```
**Note:** `[range]` can be defined as `infinity` to match to the end of the string. (See **Example 4**)

## Examples
::: details <b>Example 1</b>
#### Context:
```
The quick brown fox jumped over the lazy dog.
```
#### Input:
```
{args}
```
#### Output:
```
The quick brown fox jumped over the lazy dog.
```
:::

::: details <b>Example 2</b>
#### Context:
```
The quick brown fox jumped over the lazy dog.
```
#### Input:
```
{args;1}
```
#### Output:
```
The
```
:::

::: details <b>Example 3</b>
#### Context:
```
The quick brown fox jumped over the lazy dog.
```
#### Input:
```
{args;3;2}
```
#### Output:
```
brown fox
```
:::

::: details <b>Example 4</b>
#### Context:
```
The quick brown fox jumped over the lazy dog.
```
#### Input:
```
{args;5;infinity}
```
#### Output:
```
jumped over the lazy dog.
```
:::