---
tags:
- global
- advanced
---

# {settings}
pulls a raw Atlas setting value. If you don't understand how this tag works, then you probably shouldn't use it.

### Context Requirements
- Settings

## Syntax
```
{settings;<key>}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{settings;plugins.moderation.state}
```
#### Output:
```
enabled
```
**Note:** The output would be `disabled` if the Moderation plugin was disabled.
:::

::: details <b>Example 2</b>
#### Input:
```
{settings;plugins.moderation.logs.action}
```
#### Output:
```
457938641201922050
```
**Note:** The output would be the ID of the action log channel if it was set.
:::