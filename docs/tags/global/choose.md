---
tags:
- global
- random
---

# {choose}
returns a random parameter.

## Syntax
```
{choose;<parameterOne>;<parameterTwo>;[parameterThree];...}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{choose;thing one;thing two;thing three;thing four}
```
#### Output:
```
thing three
```
**Note:** Every time the tag is executed, the output may be different.
:::

::: details <b>Example 2</b>
#### Input:
```
I like {choose;cats;dogs;birds;reptiles}.
```
#### Output:
```
I like reptiles.
```
**Note:** Every time the tag is executed, the output may be different.
:::

::: details <b>Example 3</b>
#### Input:
```
I {choose;hate;dislike;tolerate;like;love} {choose;cats;dogs;birds;reptiles}.
```
#### Output:
```
I dislike dogs.
```
**Note:** Every time the tag is executed, the output may be different.
:::