---
tags:
- global
- casing
---

# {upper}
converts `<string>` to all uppercase. See also [{lower}](lower)

## Syntax
```
{upper;<string>}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{upper;hey i just wanted to tell you i think you're awesome}
```
#### Output:
```
HEY I JUST WANTED TO TELL YOU I THINK YOU'RE AWESOME
```
:::