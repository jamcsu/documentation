---
tags:
- global
- logic
---

# {if}
compares values. Unlike most tags, conditional parsing is enabled, meaning failed conditionals will not process.

## Syntax
```
{if;<condition>;(operator);(expression);<ifTrue>;[ifFalse]}
```
**Note:** The `(expression)` parameter is only required if the `(operator)` exists.  

## Operators
- `==`/`===` | Equal to
- `!=`/`!==` | Not equal to
- `>` | Greater than
- `>=` | Greater than or equal to
- `<` | Less than
- `<=` | Less than or equal to
- `includes` | Includes
- `startswith` | Starts with
- `endswith` | Ends with

## Examples
::: details <b>Example 1</b>
#### Input:
```
{if;{perget;yeet};true;false}
```
#### Output (if the {perget} exists):
```
true
```
#### Output (if the {perget} doesn't exist):
```
false
```
See documentation for [{perget}](perget-perset)
:::

::: details <b>Example 2</b>
#### Input:
```
{if;this;==;that;true}
```
#### Output:
```

```
**Note:** Atlas returned nothing because the statement was `false` but no `[ifFalse]` parameter was provided. 
:::

::: details <b>Example 3</b>
#### Input:
```
{if;this;==;this;true;false}
```
#### Output:
```
true
```
:::

::: details <b>Example 4</b>
#### Input:
```
{if;420;>;69;yes;no}
```
#### Output:
```
yes
```
:::

::: details <b>Example 5</b>
#### Input:
```
{if;this;===;that;doThis;{if;this;===;that;doThis;{if;this;===;this;yes;no}}}
```
#### Output:
```
yes
```
**Note:** This is an example of nested **{if}** tags. They can be nested infinitely to enable conditional parsing for an entire complicated action.
:::

::: details <b>Tip</b>
Indenting nested **{if}** tags is essential to keeping track of long and complicated code.
```
{if;this;===;that;
    doThis;
    {if;this;===;that;
        doThis;
        {if;this;===;that;
            doThis;
            {if;this;===;that;
                doThis;
                {if;this;===;that;
                    doThis;
                    doThat}}}}}
```
:::