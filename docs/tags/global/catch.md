---
tags:
- global
- error
---

# {catch}
grabs errors thrown by the `<subtags>` and returns the `<message>` instead of throwing the error.

## Syntax
```
{catch;<subtags>;<message>}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{catch;{throw;testError};Something bad happened :c}
```
#### Output:
```
Something bad happened :c
```
See documentation for [{throw}](throw)
:::

::: details <b>Example 2</b>
#### Input:
```
{catch;{user.tag;{args;1}};That user doesn't exist!}
```
#### Output (if user exists):
```
User#0000
```
#### Output (if user doesn't exist):
```
That user doesn't exist!
```
See documentation for [{user.id}](../user/user.id) and [{args}](args)
:::