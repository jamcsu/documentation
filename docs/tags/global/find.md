---
tags:
- global
- regex
---

# {find}
matches strings or patterns and returns the output. This tag also support regular expressions (regex).

## Syntax
```
{find;<target>;<search/pattern>;(group);(flags)}
```
**Note:** All `()` parameters are only required if you plan on using regex.

## Examples
::: details <b>Example 1</b>
#### Input:
```
{find;Can you find the string in this sentence?;string}
```
#### Output:
```
string
```
:::

::: details <b>Example 2</b>
#### Input:
```
{find;Can you find the test word in this sentence?;string}
```
#### Output:
```

```
**Note:** Atlas did not respond because the `<search>` was not found in the `<target>`.
:::

::: details <b>Example 3</b>
#### Input:
```
{find;Sylver's phone number is: 123-456-7890;[\d-]+}
```
**Note:** This example uses regex.
#### Output:
```
123-456-7890
```
:::

::: details <b>Example 4</b>
#### Input:
```
{find;The date is 2020/02/03;([\d+]{4})(?:\/|-)([\d+]{1,2})(?:\/|-)([\d+]{1,2});1}
```
**Note:** We defined a `(group)` parameter to only return the **first** group in the match. 
#### Output:
```
2020
```
:::

::: details <b>Example 5</b>
#### Input:
```
{find;The date is 2020/02/03;([\d+]{4})(?:\/|-)([\d+]{1,2})(?:\/|-)([\d+]{1,2});3}
```
**Note:** We defined a `(group)` parameter to only return the **third** group in the match. 
#### Output:
```
03
```
:::

::: details <b>Example 6</b>
#### Input:
```
{find;The date is 2020/02/03;([\d+]{4})(?:\/|-)([\d+]{1,2})(?:\/|-)([\d+]{1,2});-1;g}
```
**Note:** We defined a `g` global `(flag)` parameter to return the **entire** matched string. 
#### Output:
```
2020/02/03
```
:::