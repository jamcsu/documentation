---
tags:
- global
---

# {note}
returns nothing. Useful for notating the plate of spaghetti you call code.

## Examples
::: details <b>Example 1</b>
#### Input:
```
{note;This is a note, wew}
```
#### Output:
```

```
:::