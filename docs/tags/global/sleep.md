---
tags:
- global
---

# {sleep}
temporarily holds up processing. Tags before it will run as normal, tags after it will wait the defined time before executing.

## Syntax
```
{sleep;<timeout>}
```

## Examples
::: details <b>Example 1</b>
#### Input:
```
{channel.send;Before the sleep...}{sleep;10}{channel.send;After the sleep!}
```
#### Output (immediate):
```
Before the sleep...
```
#### Output (after 10 seconds):
```
After the sleep!
```
See documentation for [{channel.send}](../channel/channel.send)
:::