---
tags:
- global
- regex
---

# {replace}
looks for `<search>` in `<string>` and replaces it with `[replacement]`.

## Syntax
```
{replace;<string>;<search>;[replacement]}
```
**Note:** If `[replacement]` is not provided, it will remove `<search>` from `<string>` instead of replacing it.

## Examples
::: details <b>Example 1</b>
#### Input:
```
{replace;uwu i love to watch movies uwu;uwu}
```
#### Output:
```
i love to watch movies
```
:::

::: details <b>Example 2</b>
#### Input:
```
{replace;uwu i love to watch movies uwu;uwu;REEE}
```
#### Output:
```
REEE i love to watch movies REEE
```
:::

::: details <b>Example 3</b>
#### Input:
```
{replace;The quick brown fox jumped over the lazy dog;([A-z]+);($1)}
```
#### Output:
```
(The) (quick) (brown) (fox) (jumped) (over) (the) (lazy) (dog)
```
:::