---
tags:
- global
---

# {r}
inserts a right curly bracket (`}`) that won't get eaten by the parser. Useful if you need one in a string or sentence.

## Examples
::: details <b>Example 1</b>
#### Input:
```
You can write {l}tags{r} without them being parsed!
```
#### Output:
```
You can write {tags} without them being parsed!
```
See documentation for [{l}](l)
:::