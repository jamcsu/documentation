---
tags:
- global
---

# {delete}
deletes the message in context. If you want to delete a specific message, use [{message.delete}](../message/message.delete)

### Context Requirements
- Message

## Examples
::: details <b>Example 1</b>
#### Context:
```
a!command
```
#### Input:
```
{delete}
```
#### Output:
```

```
**Note:** The tag outputs nothing but would delete the invocation message.
:::