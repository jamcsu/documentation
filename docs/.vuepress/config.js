module.exports = {
    
    dest: 'public',
    
    title: 'Atlas Documentation',

    head: [
        ['link', { rel: 'icon', href: '/favicon.png' }]
      ],

    base: '/documentation/',

    description: 'Official documentation for the Atlas bot.',

    lastUpdated: 'Last Updated',

    plugins: [
      ['@vuepress/back-to-top'],

      ['code-copy', {
        align: 'top',
        color: '#ffffff80',
        backgroundTransition: false
      }]
    ],
    
    themeConfig: {

        logo: '/logo.svg',

        sidebar: {
            '/tags/': [
                {
                    title: 'Global',
                    collapsable: true,
                    children: [
                      'global/args',
                      'global/catch',
                      'global/choose',
                      'global/delete',
                      'global/find',
                      'global/length',
                      'global/math',
                      'global/note',
                      'global/randstr',
                      'global/range',
                      'global/replace',
                      'global/settings',
                      'global/sleep',
                      'global/throw',

                      {
                        title: 'Logic',
                        collapsable: true,
                        children: [
                          'global/and',
                          'global/if',
                          'global/or'
                        ]
                      },

                      {
                        title: 'Variables',
                        collapsable: true,
                        children: [
                          {
                            title: 'Temporary',
                            collapsable: false,
                            children: [
                              'global/set',
                              'global/get'
                            ]
                          },

                          {
                            title: 'Permanent',
                            collapsable: false,
                            children: [
                              'global/perset',
                              'global/perget'
                            ]
                          }
                        ]
                      },

                      {
                        title: 'Casing',
                        collapsable: true,
                        children: [
                          'global/upper',
                          'global/lower'
                        ]
                      },

                      {
                        title: 'Characters',
                        collapsable: true,
                        children: [
                          'global/l',
                          'global/r',
                          'global/semi'
                        ]
                      }
                    ]
                  },

                  {
                    title: 'Channel',
                    collapsable: true,
                    children: [
                      'channel/channel.awaitmessage',
                      'channel/channel.createdat',
                      'channel/channel.id',
                      'channel/channel.lastmessageid',
                      'channel/channel.mention',
                      'channel/channel.name'
                    ]
                  }
              ],

            '/help/': [
                {
                    title: 'FAQ',
                    path: '/help/faq/',
                    collapsable: false
                  },

                {
                    title: 'Tutorials',
                    collapsable: false,
                    children: [
                      {
                        title: 'Actions',
                        children: [
                          'tutorials/actions/introduction'
                        ]
                      }
                    ]
                  }
              ]
          },

        nav: [
            {
              text: 'Go to',
              items: [
                { text: 'Documentation', items: [
                  {text: 'Tags', link: '/tags/'},
                  {text: 'Tutorials', link: '/help/tutorials/'},
                  {text: 'FAQ', link: '/help/faq/'}
              ] },
                { text: 'Dashboard', items: [
                    {text: 'Home', link: 'https://atlasbot.xyz'},
                    {text: 'My Servers', link: 'https://atlasbot.xyz/@me/guilds'},
                ] }
              ]
            },
            { text: 'Support Server', link: 'https://discord.gg/AXXBPM7'}
          ],

        searchMaxSuggestions: 10,
        searchPlaceholder: 'Search the docs...',

        nextLinks: true,
        prevLinks: true,

        repo: 'https://gitlab.com/jamcsu/documentation',
        docsDir: 'docs',
        editLinks: true,
        editLinkText: 'Edit this Page',

        smoothScroll: true
    }
}
